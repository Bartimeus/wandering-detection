import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

const styles = StyleSheet.create({
  container: {
    maxHeight: 40,
    maxWidth: 40,
    borderRadius: 40 / 2,
    backgroundColor: 'transparent',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  letter: {
    fontWeight: 'bold',
    fontSize: 34,
    color: 'white',
  },
});

interface IProps {
  letter: string;
}

export default function LetterIcon({ letter }: IProps) {

  styles.container = {
    ...styles.container,
    backgroundColor: 'red',
  };

  return (
    <View style={styles.container}>
      <Text style={styles.letter}>{letter}</Text>
    </View>
  );
}
