import React, { Dispatch, SetStateAction } from 'react';
import { FlatList, StyleSheet, View } from 'react-native';

import { IUser } from '../../../../../types/User';
import UserListItem from '../UserListItem/Component';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },
});

interface IProps {
  users: IUser[] | null;
  viewingUser: IUser | undefined;
  startViewingUser(user: IUser): void;
  finishViewingUser(): void;
  startEditingUser(user: IUser): void;
}

export default function UserListView(props: IProps) {
  return (
    <View style={styles.container}>
      <FlatList
        data={props.users}
        extraData={{ viewingUser: props.viewingUser }}
        keyExtractor={item => item.id.toString()}
        renderItem={({ item }) => {
          return (
            <UserListItem
              user={item}
              viewingUser={props.viewingUser}
              startEditingUser={props.startEditingUser}
              startViewingUser={props.startViewingUser}
              finishViewingUser={props.finishViewingUser}
            />);
        }}
      />
    </View>
  );
}
