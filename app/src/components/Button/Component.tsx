import React from 'react';
import {
  GestureResponderEvent,
  StyleProp,
  StyleSheet,
  Text,
  TextStyle,
  TouchableOpacity,
  ViewStyle
} from 'react-native';

interface IButtonProps {
  onPress: ((event: GestureResponderEvent) => void);
  accessibilityLabel: string;
  accessibilityHint: string;
  children?: string;
  type?: 'solid' | 'outline' | 'clear';
  shape?: 'square' | 'round';
  color?: string;
  buttonStyle?: StyleProp<ViewStyle>;
  textStyle?: StyleProp<TextStyle>;
}

const styles = {
  defaultButton: {
    backgroundColor: '#4287d6',
    color: '#fff',
    padding: 8,
    paddingLeft: 16,
    paddingRight: 16,
    borderRadius: 4,
  },
  text: {
    color: '#fff',
    fontSize: 18,
  }
};

export default function Button(props: IButtonProps) {
  let buttonStyle = {};
  let textStyle = {};

  if (props.type) {
    if (props.type === 'outline' || props.type === 'clear') {
      const color = typeof props.color !== 'undefined' ? props.color : '#4287d6';
      textStyle = { ...textStyle, color };
      buttonStyle = { ...buttonStyle, backgroundColor: 'none' };
      if (props.type === 'clear') {
        buttonStyle = {
          ...buttonStyle,
          borderWidth: 1,
          borderColor: color,
        };
      }
    }
  }

  if (props.shape) {
    if (props.shape === 'round') {
      // We create the border radius to a high number, because we don't know
      // what the height of the button will be. In react native it is not
      // possible to work with percentages.
      buttonStyle = { ...buttonStyle, borderRadius: 99999999 };
    }
  }

  return (
    <TouchableOpacity
      onPress={props.onPress}
      style={StyleSheet.flatten([styles.defaultButton, buttonStyle, props.buttonStyle])}>
      <Text style={StyleSheet.flatten([styles.text, textStyle, props.textStyle])}>
        {props.children}
      </Text>
    </TouchableOpacity>
  );
}