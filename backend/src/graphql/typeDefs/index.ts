import BaseDef from './BaseDefs';
import ClientDef from './ClientDef';
import LocationDef from './LocationDef';

export default [BaseDef, ClientDef, LocationDef];
