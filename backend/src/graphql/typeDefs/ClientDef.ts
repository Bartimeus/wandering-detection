import { gql } from 'apollo-server-express';

export default gql`
  type Query {
    getClients: Client
  }

  type Client {
    client_id: String!
    firstName: String!
    latName: String!
    profile_pictore: String
    created_on: DateTime!
    location: [Location]
  }
`;
