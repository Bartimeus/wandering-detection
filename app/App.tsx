import React, { useRef } from 'react';
import { ApolloProvider } from '@apollo/client';

import initApollo, { graphqlUrl } from './src/api/create-apollo-client';
import AppContainer from './src/AppContainer';

export default function AppComponent() {
  const apolloClient = useRef(
    initApollo({
      introspectionResult: undefined,
      baseUrl: graphqlUrl,
    }),
  );
  return (
    <ApolloProvider client={apolloClient.current}>
      <AppContainer />
    </ApolloProvider>
  );
}
