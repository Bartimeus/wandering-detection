import React from 'react';
import { Animated, StyleSheet, Text, View } from 'react-native';
import SlidingUpPanel from 'rn-sliding-up-panel';

import { IUser } from '../../../../types/User';
import BottomSheet, { bottomSheetStyles, IBottomSheetProps, IBottomSheetState } from '../Component';
import Nodge from '../Nodge/Component';
import NoUsers from './NoUsers/Component';
import UserListView from './UserListView/Component';

const styles = StyleSheet.create({
  content: {
    flex: 1,
    justifyContent: 'flex-start',
  },
  panel: {
    flex: 1,
    backgroundColor: 'white',
    position: 'relative',
    borderRadius: 10,
  },
  panelHeader: {
    height: 80,
    justifyContent: 'flex-end',
    padding: 24,
  },
  textHeader: {
    fontSize: 28,
    color: 'black',
  },
});

export interface IUserListProps extends IBottomSheetProps {
  title: string;
  clients: IUser[];
  viewingUser: IUser | undefined;
  startViewingUser(user: IUser): void;
  finishViewingUser(): void;
  startEditingUser(user: IUser): void;
}

export default class UserList extends BottomSheet<IUserListProps, IBottomSheetState> {
  public render() {
    // TODO: Proper rendering of the no user style.
    if (this.props.clients.length <= 0) {
      return <View style={this._containerStyles}>< NoUsers /></View>;
    }

    const { top, bottom } = this.state.draggableRange;
    styles.panelHeader.height = top;

    const textTranslateY = this._animatedValue.interpolate({
      inputRange: [bottom, top],
      outputRange: [0, 20],
      extrapolate: 'clamp',
    });

    const textTranslateX = this._animatedValue.interpolate({
      inputRange: [bottom, top],
      outputRange: [0, -50],
      extrapolate: 'clamp',
    });

    const textScale = this._animatedValue.interpolate({
      inputRange: [bottom, top],
      outputRange: [1, 0.7],
      extrapolate: 'clamp',
    });

    const panelHeaderStyle = {
      transform: [
        { translateY: textTranslateY },
        { translateX: textTranslateX },
        { scale: textScale },
      ],
    };

    return (
      <View style={this._containerStyles}>
        <SlidingUpPanel
          showBackdrop={false}
          height={this._height}
          containerStyle={bottomSheetStyles.bottomSheetStyling}
          draggableRange={this.state.draggableRange}
          animatedValue={this._animatedValue}
          allowDragging={true}
          ref={c => this._slidingUpPanel = c}>
          <View style={styles.panel}>
            <Nodge />
            <View style={styles.panelHeader}>
              <Animated.View style={panelHeaderStyle}>
                <Text style={styles.textHeader}>{this.props.title}</Text>
              </Animated.View>
            </View>
            <UserListView
              users={this.props.clients}
              viewingUser={this.props.viewingUser}
              startViewingUser={this.props.startViewingUser}
              finishViewingUser={this.props.finishViewingUser}
              startEditingUser={this.props.startEditingUser} />
          </View>
        </SlidingUpPanel >
      </View>);
  }
}