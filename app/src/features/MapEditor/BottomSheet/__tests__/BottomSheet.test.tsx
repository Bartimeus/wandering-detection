/**
 * @format
 */

import React from 'react';
import 'react-native';
import renderer from 'react-test-renderer';
import { IUser } from '../../../../types/User';
import BottomSheet from '../Component';
import SurroundingEdit from '../SurroundingEdit/Component';
import UserList from '../UserList/Component';

describe('BottomSheet', () => {
  it('default bottomSheet renders correctly', () => {
    const bottomSheet = renderer
      .create(
        <BottomSheet panelHeight={100} />,
      )
      .toJSON();
    expect(bottomSheet).toMatchSnapshot();
  });

  const user1: IUser = {
    id: 1,
    firstname: 'Voornaam',
    lastname: 'Achternaam',
    currentLocation: undefined,
    surrounding: undefined,
    profilePicture: undefined
  };

  const user2 = {
    id: 2,
    firstname: 'Voornaam2',
    lastname: 'Achternaam2',
    currentLocation: undefined,
    surrounding: undefined,
    profilePicture: undefined
  };

  describe('UserList', () => {
    const userList = renderer.create(
      <UserList
        panelHeight={100}
        title="Cliënten"
        clients={[]}
        viewingUser={undefined}
        startEditingUser={(user: IUser) => { /* Do something */ }}
        startViewingUser={(user: IUser) => { /* Do something */ }}
        finishViewingUser={() => { /* Do something */ }}
      />
    );

    it('empty userlist bottomSheet renders correctly', () => {
      userList.toJSON();
      expect(userList).toMatchSnapshot();
    });
  });

  describe('SurroundingEdit', () => {
    const surroundingEdit = renderer.create(
      <SurroundingEdit
        panelHeight={100}
        editingUser={user1}
        finishEditingUser={(user: IUser) => { /* Do something */ }}
        cancelEditingUser={() => { /* Do something */ }}
        resetEditingUser={() => { /* Do something */ }}
      />
    );

    it('empty surroundingEdit bottomSheet renders correctly', () => {
      surroundingEdit.toJSON();
      expect(surroundingEdit).toMatchSnapshot();
    });
  });

});
