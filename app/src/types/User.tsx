import LatLng from './LatLng';
import Surrounding from './surrounding';

export interface IUser {
  id: number;
  firstname: string;
  lastname: string;
  currentLocation: LatLng | undefined;
  surrounding: Surrounding | undefined;
  profilePicture: string | undefined;
}
