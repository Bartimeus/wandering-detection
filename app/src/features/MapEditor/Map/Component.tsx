import React from 'react';
import { Dimensions, StyleSheet, View } from 'react-native';
import MapView, { MapEvent, Polygon, UrlTile } from 'react-native-maps';
import LatLng from '../../../types/LatLng';
import { IUser } from '../../../types/User';

const { height, width } = Dimensions.get('window');
const ASPECT_RATIO = width / height;

interface IProps {
  editingUser: IUser | undefined;
  viewingUser: IUser | undefined;
  editingUserLocations: LatLng[];
  newPointHandler(event: MapEvent): void;
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});

function MapComponent(props: IProps) {
  const region = {
    latitude: 52.048255,
    longitude: 5.3134138,
    latitudeDelta: 0.01,
    longitudeDelta: 0.01 * ASPECT_RATIO,
  };

  let renderPolygon;
  if (props.viewingUser && props.viewingUser.surrounding) {
    renderPolygon = (
      <Polygon
        coordinates={props.viewingUser.surrounding.locations.slice()}
        strokeColor="#000"
        fillColor="rgba(255,0,0,0.5)"
        strokeWidth={1}
      />
    );
  }

  if (typeof props.editingUser !== 'undefined') {
    renderPolygon = (
      <Polygon
        coordinates={props.editingUserLocations.slice()}
        strokeColor="#000"
        fillColor="rgba(255,0,0,0.5)"
        strokeWidth={1}
      />
    );
  }

  return (
    <View style={styles.container}>
      <MapView
        style={styles.map}
        initialRegion={region}
        onPress={props.newPointHandler}
        maxZoomLevel={17}
      >
        <UrlTile
          urlTemplate="http://c.tile.openstreetmap.org/{z}/{x}/{y}.png"
          maximumZ={19}
        />
        {renderPolygon}
      </MapView>
    </View>
  );
}

export default MapComponent;