module.exports = {
  root: true,
  parser: '@typescript-eslint/parser',
  parserOptions: {
    project: ['./backend/tsconfig.json'],
  },
  plugins: ['@typescript-eslint'],
  extends: ['airbnb-typescript', 'airbnb/hooks', 'prettier/@typescript-eslint', 'prettier/react', 'plugin:prettier/recommended'],
  rules: {
    'react/destructuring-assignment': [2, 'never'],
    'import/no-unused-modules': 0,
  },
  env: {
    browser: true,
    node: true,
    jest: true,
  },
  overrides: [
    {
      files: ['**/*.test.js'],
      env: {
        jest: true, // now **/*.test.js files' env has both es6 *and* jest
      },
      // Can't extend in overrides: https://github.com/eslint/eslint/issues/8813
      // "extends": ["plugin:jest/recommended"]
      plugins: ['jest'],
      rules: {
        'jest/no-disabled-tests': 'warn',
        'jest/no-focused-tests': 'error',
        'jest/no-identical-title': 'error',
        'jest/prefer-to-have-length': 'warn',
        'jest/valid-expect': 'error',
      },
    },
  ],
};
