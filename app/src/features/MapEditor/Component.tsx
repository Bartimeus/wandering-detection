import React, { Fragment, useEffect, useRef, useState } from 'react';

import { View } from 'react-native';
import { MapEvent } from 'react-native-maps';
import { getClientsHook } from '../../api/clients';
// import { useCreateSurroundingMutation, useGetClientsQuery } from '../../api/graphql/_generated_graphql_types';
import LatLng from '../../types/LatLng';
import { IUser } from '../../types/User';
import BottomSheet, { IBottomSheetState } from './BottomSheet/Component';
import SurroundingEdit, { ISurroundingEditProps } from './BottomSheet/SurroundingEdit/Component';
import UserList, { IUserListProps } from './BottomSheet/UserList/Component';
import Map from './Map/Component';

export default function MapEditor() {
  // const getClientQuery = useGetClientsQuery();
  const [editingUser, setEditingUser] = useState<IUser | undefined>(undefined);
  const [viewingUser, setViewingUser] = useState<IUser | undefined>(undefined);
  // const [surroundingMutation] = useCreateSurroundingMutation();
  const [editingUserLocations, setEditingUserLocations] = useState<LatLng[]>([]);
  const [clients, setClients] = useState<IUser[] | []>([]);
  const userListBottomSheet = useRef<BottomSheet<IUserListProps, IBottomSheetState>>(null);
  const surroundingEditBottomSheet = useRef<BottomSheet<ISurroundingEditProps, IBottomSheetState>>(null);

  // useEffect(() => {
  //   setClients(getClientsHook(getClientQuery));
  // }, [useGetClientsQuery()]);

  /**
   * Start editing the given user.
   * @param {IUser} user - The user that is beign editted.
   */
  const startEditingUser = async (user: IUser) => {
    if (userListBottomSheet && userListBottomSheet.current) {
      userListBottomSheet.current.openBottomSheet(false, 0).then(() => {
        setEditingUser(user);
        setViewingUser(undefined);
        if (user.surrounding) {
          setEditingUserLocations(user.surrounding.locations);
        }
      });
    }
  };

  /**
   * Save the new user surrounding.
   */
  const finishEditingUser = async () => {
    // Make sure some variables are set.
    if (typeof editingUser === 'undefined' || editingUserLocations.length < 2) {
      // TODO: error handling when something is not right.
      return;
    }

    const requestBody = {
      variables: {
        polygon: {
          type: 'Polygon',
          coordinates: [
            editingUserLocations.map(point => [point.longitude, point.latitude]),
          ],
        },
        clientId: editingUser.id,
      },
    };

    try {
      // await surroundingMutation(requestBody);
      cancelEditingUser();
      // getClientQuery.refetch();
    } catch (e) {
      // TODO: proper error handling.
    }
  };

  /**
   * Close surrounding Edit Bottomsheet and reset mapEditor.
   */
  const cancelEditingUser = () => {
    if (surroundingEditBottomSheet && surroundingEditBottomSheet.current) {
      surroundingEditBottomSheet.current.openBottomSheet(false, 0).then(() => {
        setEditingUser(undefined);
        setEditingUserLocations([]);
      });
    }
  };

  /**
   * Remove all points, but keep editing the user.
   */
  const resetEditingUser = () => {
    setEditingUserLocations([]);
  };

  /**
   * Handler which is being used to put points on the map.
   * @param {MapEvent} event - Map press event. 
   */
  const newPointHandler = (event: MapEvent) => {
    // Only works when editing or creating.
    if (!editingUser) {
      return;
    }

    const coordinates = event.nativeEvent.coordinate;
    setEditingUserLocations(
      [
        ...editingUserLocations,
        new LatLng(coordinates.latitude, coordinates.longitude)
      ]
    );
  };

  /**
   * Start viewing the given user.
   * @param {IUser} user - The user that is beign viewed
   */
  const startViewingUser = (user: IUser) => {
    setViewingUser(user);
    if (userListBottomSheet && userListBottomSheet.current) {
      userListBottomSheet.current.openBottomSheet(true, 100);
    }
  };

  /**
   * Stop viewing the user.
   */
  const finishViewingUser = () => {
    setViewingUser(undefined);
    if (userListBottomSheet && userListBottomSheet.current) {
      userListBottomSheet.current.openBottomSheet(true, 100);
    }
  };

  let content = <UserList
    title="Cliënten"
    ref={userListBottomSheet}
    panelHeight={100}
    clients={clients}
    viewingUser={viewingUser}
    startViewingUser={startViewingUser}
    finishViewingUser={finishViewingUser}
    startEditingUser={startEditingUser} />;

  if (typeof editingUser !== 'undefined') {
    content = <SurroundingEdit
      ref={surroundingEditBottomSheet}
      panelHeight={100}
      editingUser={editingUser}
      finishEditingUser={finishEditingUser}
      cancelEditingUser={cancelEditingUser}
      resetEditingUser={resetEditingUser} />;
  }

  useEffect(() => {
    if (userListBottomSheet && userListBottomSheet.current) {
      userListBottomSheet.current.initBottomSheet();
    }
    if (surroundingEditBottomSheet && surroundingEditBottomSheet.current) {
      surroundingEditBottomSheet.current.initBottomSheet();
    }
  });

  return (
    <Fragment>
      <Map
        editingUser={editingUser}
        viewingUser={viewingUser}
        newPointHandler={newPointHandler}
        editingUserLocations={editingUserLocations} />
      <View>
        {content}
      </View>
    </Fragment>
  );
}
