import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import SlidingUpPanel from 'rn-sliding-up-panel';
import Button from '../../../../components/Button/Component';
import { IUser } from '../../../../types/User';
import BottomSheet, { bottomSheetStyles, IBottomSheetProps, IBottomSheetState } from '../Component';

const styles = StyleSheet.create({
  panel: {
    flex: 1,
    backgroundColor: 'white',
    position: 'relative',
    borderRadius: 10,
  },
  panelHeader: {
    height: 100,
    justifyContent: 'flex-start',
    paddingLeft: 24,
    paddingTop: 18,
  },
  textHeader: {
    fontSize: 21,
    color: 'black',
  },
  buttons: {
    margin: 0,
    paddingTop: 10,
    alignItems: 'flex-start',
    flexDirection: 'row'
  },
  buttonFirstChild: {
    marginRight: 5,
  },
  button: {
    marginLeft: 5,
    marginRight: 5,
  }
});

export interface ISurroundingEditProps extends IBottomSheetProps {
  editingUser: IUser;
  finishEditingUser(user: IUser): void;
  cancelEditingUser(): void;
  resetEditingUser(): void;
}

export default class SurroundingEdit extends BottomSheet<ISurroundingEditProps, IBottomSheetState> {
  public render() {
    return (
      <View style={this._containerStyles}>
        <SlidingUpPanel
          showBackdrop={false}
          height={this._height}
          containerStyle={bottomSheetStyles.bottomSheetStyling}
          draggableRange={this.state.draggableRange}
          animatedValue={this._animatedValue}
          allowDragging={false}
          ref={c => this._slidingUpPanel = c}>
          <View style={styles.panel}>
            <View style={styles.panelHeader}>
              <Text style={styles.textHeader}>Bewerken {this.props.editingUser.firstname}</Text>
              <View style={styles.buttons}>
                <Button
                  onPress={() => this.props.finishEditingUser(this.props.editingUser)}
                  shape="round"
                  buttonStyle={styles.buttonFirstChild}
                  accessibilityLabel="Sla gebruiker op"
                  accessibilityHint="Slaat gebruiker op en keert terug naar overzicht">
                  Opslaan
                </Button>
                <Button
                  onPress={() => this.props.resetEditingUser()}
                  type="clear"
                  shape="round"
                  buttonStyle={styles.button}
                  accessibilityLabel="Haal bestaande omgeving weg"
                  accessibilityHint="Haalt alle bestaande punten van de cliënt weg en je blijft bewerken">
                  Reset
                </Button>
                <Button
                  onPress={() => this.props.cancelEditingUser()}
                  type="clear"
                  shape="round"
                  color="#ff0000"
                  buttonStyle={styles.button}
                  accessibilityLabel="Terug naar overzicht"
                  accessibilityHint="Slaat gebruiker niet op en keert terug naar overzicht">
                  Cancel
                </Button>
              </View>
            </View>
          </View>
        </SlidingUpPanel >
      </View>);
  }
}