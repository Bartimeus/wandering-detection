import LatLng from './LatLng';

export interface ISurrounding {
  locations: LatLng[];
}

export default class Surrounding implements ISurrounding {
  public locations: LatLng[] = [];
}
