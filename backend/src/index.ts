import express from 'express';
import { ApolloServer } from 'apollo-server-express';

import typeDefs from './graphql/typeDefs';
import resolvers from './graphql/resolvers';

const port = process.env.PORT || 4000;
// const isProd = process.env.NODE_ENV === 'production';

const app = express();
const server = new ApolloServer({ typeDefs, resolvers });

server.applyMiddleware({ app });

app.listen(port, () => {
  // eslint-disable-next-line no-console
  console.log(`Server is ready on  http://localhost:${port}`);
  // eslint-disable-next-line no-console
  console.log(`Graphql is ready on http://localhost:${port}/graphql`);
});

// // In the future we aren't going to use node scheduler,
// // instead we are going to do this on receiving data
// // from the things network.
// schedule.scheduleJob('*/5 * * * * *', async () => {
//   try {
//     const clientRes = await pool.query('select * from wandering_public.client');
//     const clients = clientRes.rows;

//     const userOutsideSurrounding = await Promise.all(
//       clients.map(async (client) => {
//         const userInSurrounding = await pool.query(`SELECT
//           l.location_id,
//           (SELECT
//             COUNT(*)
//             FROM wandering_public.surrounding AS s
//             WHERE ST_Intersects(l.location, s.polygon) AND
//             s.client_id = ${client.client_id}
//             ORDER BY l.created_on DESC
//             LIMIT 1)
//         FROM wandering_public.location AS l
//         WHERE l.client_id = ${client.client_id}
//         ORDER BY l.created_on DESC
//         LIMIT 1;`);

//         if (userInSurrounding.rowCount <= 0) {
//           throw 'User has no location yet.';
//         }

//         return {
//           clientId: client.client_id,
//           inSurrounding: userInSurrounding.rows[0].count > 0
//         };
//       })
//     );

//     // TODO: Send user notification when client is outside of polygon.
//   } catch (exception) {
//     console.error(exception);
//   }
// });
