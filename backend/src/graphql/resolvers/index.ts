import merge from 'smart-merge';
import Client from './clientResolver';
import Location from './locationResolver';

export default merge(Client, Location);
