/* eslint-disable import/no-extraneous-dependencies */
const airbnbStyleRules = require('eslint-config-airbnb-base/rules/style').rules;
const airbnbVariablesRules = require('eslint-config-airbnb-base/rules/variables').rules;

module.exports = {
  extends: [
    '@react-native-community',
    require.resolve('eslint-config-airbnb'),
    require.resolve('eslint-config-prettier'),
    require.resolve('eslint-config-prettier/react')
  ],
  plugins: [ 'react-hooks', 'prettier' ],
  parser: require.resolve('babel-eslint'),
  env: {
    browser: true
  },
  settings: {
    'import/parsers': {
      [require.resolve('@typescript-eslint/parser')]: [ '.ts', '.tsx', '.d.ts' ]
    },
    'import/resolver': {
      node: {
        extensions: [ '.ts', '.tsx', '.d.ts', '.js', '.jsx' ]
      }
    }
  },
  rules: {
    'prettier/prettier': [
      'error',
      {
        printWidth: 80,
        proseWrap: 'preserve',
        quoteProps: 'as-needed',
        singleQuote: true,
        trailingComma: 'all'
      }
    ],

    // Add react hooks checking. This makes sure you use React hooks the right
    // way.
    'react-hooks/rules-of-hooks': 'error',
    'react-hooks/exhaustive-deps': 'error',

    // Disable prefer-default-export. Maybe we should actually discourage
    // default exports, because named exports sometimes have a better DX. Until
    // we've decided whether we want this or not, we shouldn't give errors for
    // it. For more info, see:
    // https://blog.neufund.org/why-we-have-banned-default-exports-and-you-should-do-the-same-d51fdc2cf2ad
    'import/prefer-default-export': [ 0 ],

    // Normally, you shouldn't use stuff that starts or ends with an _. We have
    // a few exemptions for common stuff used in projects.
    'no-underscore-dangle': [
      'error',
      {
        allow: [
          '__hn', // Headless Ninja
          '_paq', // Matomo tracking
          '__typename' // GraphQL queries
        ]
      }
    ],

    // Styled components uses tagged templates for their injectGlobal function:
    // injectGlobal`
    //   body{margin:0}
    // `
    // That is disallowed by default, because there is no variable assignment.
    // By setting allowTaggedTemplates to true we ignore this, however if we
    // migrate to Styled Components v4 we should use 'createGlobalStyle' which
    // does use varible assignment. Then we can remove this exception here.
    'no-unused-expressions': [ 'error', { allowTaggedTemplates: true } ],

    // PropTypes are like a poor-mans version of Typescript. They ensure some
    // way of type safety, but the proptypes errors only show up in the console
    // of the browser, and they don't show up if components don't have them
    // defined at all. That's why for now PropTypes checking will be disabled
    // on components that don't have PropTypes now. Creating PropTypes for HOCs
    // is cruesome, and we should think of an easier way before enabling this
    // rule again. React hooks as an alternative for props can be a solution,
    // but that should also make it easier to migrate to Typescript.
    'react/prop-types': [ 'error', { skipUndeclared: true } ],

    // Functions are hoisted, which makes them safe to use before their
    // declaration. It's most of the times useful to put the most important part
    // of the code on top of the file, which means that utility functions are
    // lower in the file. Disabling this rule for functions makes that possible.
    'no-use-before-define': [ 'error', { functions: false } ],

    // Because Prettier adds some nice formatting to ternary expressions, we can
    // use them without getting overwelmed. They are great because they can be
    // used as switches, without the switch-case-break overhead.
    'no-nested-ternary': 0,

    // With hooks & TypeScript coming into play, it's not always better to
    // destructure the props of a component. It makes it harder to see the
    // difference between state / context / props, and combined typings get lost
    // when the props are destructured. Maybe in the future we will require the
    // inverse, so by disallowing destructuring props. For now, we don't lint
    // it.
    'react/destructuring-assignment': 0,
    // In a future, we can upgrade to eslint-plugin-import@latest and add
    // eslint-plugin-import/config/typescript to the extends section.
    'import/extensions': [
      'error',
      'ignorePackages',
      {
        js: 'never',
        jsx: 'never',
        ts: 'never',
        tsx: 'never',
        mjs: 'never'
      }
    ],
    'no-console': [ 'error', { allow: [ 'warn', 'error' ] } ]
  },
  overrides: [
    {
      // For Typescript, we need to have some special handling. We use the
      // @typescript-eslint parser and plugin to lint Typescript files. These
      // settings were partially copied from:
      // https://github.com/iamturns/eslint-config-airbnb-typescript/blob/f2b4d239be2e2780284307cf19764bc402201484/index.js
      files: [ '*.ts', '*.tsx' ],
      parser: require.resolve('@typescript-eslint/parser'),
      plugins: [ '@typescript-eslint' ],
      rules: {
        // These rules are in the airbnb styleguide, but we replace them with
        // their TypeScript-specific equivalents.
        camelcase: 0,
        'no-array-constructor': 0,
        'no-unused-vars': 0,
        'no-useless-constructor': 0,
        '@typescript-eslint/camelcase': airbnbStyleRules.camelcase,
        '@typescript-eslint/no-array-constructor': airbnbStyleRules['no-array-constructor'],
        '@typescript-eslint/no-unused-vars': airbnbVariablesRules['no-unused-vars'],
        '@typescript-eslint/no-useless-constructor': airbnbStyleRules['no-useless-constructor'] || 0,
        'no-empty-function': [ 'error', { allow: [ 'constructors' ] } ],

        // These are verified by TypeScript itself, and shouldn't be verified
        // by ESLint.
        'import/no-unresolved': 0,
        'import/named': 0,
        'react/jsx-filename-extension': 0,

        // Prefer function types over interfaces.
        '@typescript-eslint/prefer-function-type': 'error',

        // In TypeScript it's very common to implement an interface with
        // methods. These methods don't always use 'this', so it's disabled for
        // typescript.
        'class-methods-use-this': 0,

        // In TypeScript, it's possible to overload a class method. ESLint gives
        // a false positive when we're doing that, so we disable this rule.
        'no-dupe-class-members': 0,

        // In TypeScript, return values are automatically inferrerd and checked,
        // so we don't need consistent returns.
        'consistent-return': 0,

        // If you want to ignore (and therefore, not type) a function argument,
        // you can use an empty destructuring pattern. So, something like:
        // function test({}, text: string) {console.log(text);}
        // It's however forbidden by this eslint rule, so that's why we disable
        // it.
        'no-empty-pattern': 0,

        // This rule gives a false positives when only importing types from
        // modules, so it's disabled for now.
        'import/no-cycle': 0,

        'import/no-named-as-default': 0
      }
    }
  ]
};
