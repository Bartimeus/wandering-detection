import {
  ApolloClient,
  InMemoryCache,
  NormalizedCacheObject,
} from '@apollo/client';

let apolloClient: ApolloClient<NormalizedCacheObject> | null = null;

export const graphqlUrl = 'http://403b4bad.ngrok.io';

function createApollo(
  options: { baseUrl: string; introspectionResult: any } = {
    baseUrl: '',
    introspectionResult: undefined,
  },
) {
  return new ApolloClient<NormalizedCacheObject>({
    uri: `${options.baseUrl || ''}/graphql`,
    cache: new InMemoryCache({
      // fragmentMatcher: new IntrospectionFragmentMatcher({
      //   introspectionQueryResultData: options.introspectionResult,
      // }),
    }),
  });
}

export default function initApollo(
  options: { baseUrl: string; introspectionResult: any } = {
    baseUrl: '',
    introspectionResult: undefined,
  },
) {
  if (!apolloClient) {
    apolloClient = createApollo(options);
  }

  return apolloClient;
}
