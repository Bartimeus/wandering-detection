import React, { Dispatch, SetStateAction, useContext } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Button from '../../../../../components/Button/Component';
import LetterIcon from '../../../../../components/LetterIcon/Component';
import { IUser } from '../../../../../types/User';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingBottom: 5,
    paddingTop: 5,
    paddingLeft: 18,
    paddingRight: 18,
    marginTop: 2,
    marginBottom: 2
  },
  selectedContainer: {
    backgroundColor: 'rgba(0, 122, 255, 0.2)'
  },
  nameContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  fullName: {
    marginLeft: 10,
    fontSize: 16,
    fontWeight: 'bold'
  },
  icon: {
    borderRadius: 22,
    borderColor: '#DCDCDC',
    borderWidth: 2,
    borderStyle: 'solid'
  },
  button: {
    backgroundColor: 'rgb(0, 122, 255)',
    color: 'white',
    borderRadius: 4,
    overflow: 'hidden',
    paddingTop: 8,
    paddingBottom: 8,
    paddingLeft: 16,
    paddingRight: 16,
    fontSize: 16
  }
});

interface IProps {
  user: IUser;
  viewingUser: IUser | undefined;
  startViewingUser(user: IUser): void;
  startEditingUser(user: IUser): void;
  finishViewingUser(): void;
}

function UserListItem(props: IProps) {
  const buttonText = props.user.surrounding ? 'bewerk' : 'nieuw';
  const editUser = () => {
    props.startEditingUser(props.user);
  };

  const containerClasses: any[] = [styles.container];
  if (
    props.viewingUser &&
    props.user &&
    props.viewingUser.id === props.user.id
  ) {
    containerClasses.push(styles.selectedContainer);
  }

  return (
    <TouchableOpacity onPress={() => {
      toggleSelectedUser(props.user, props.viewingUser, props.startViewingUser, props.finishViewingUser);
    }} activeOpacity={1}>
      <View style={containerClasses}>
        <View style={styles.nameContainer}>
          <LetterIcon letter={props.user.firstname.substr(0, 1)} />
          <Text style={styles.fullName}>{`${props.user.firstname} ${props.user.lastname}`}</Text>
        </View>
        <Button
          onPress={editUser}
          accessibilityLabel={`Bewerk ${props.user.firstname}`}
          accessibilityHint={`Bewerk de omgeving van ${props.user.firstname}`}>
          {buttonText}
        </Button>
      </View>
    </TouchableOpacity>
  );
}

function toggleSelectedUser(
  user: IUser,
  viewingUser: IUser | undefined,
  startViewingUser: (user: IUser) => void,
  finishEditingUser: () => void,
) {
  if (typeof viewingUser === 'undefined' || (viewingUser && viewingUser.id !== user.id)) {
    startViewingUser(user);
  } else {
    finishEditingUser();
  }
}

export default UserListItem;
