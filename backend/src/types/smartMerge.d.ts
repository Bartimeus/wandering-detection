declare module 'smart-merge' {
  interface MergedObject {
    [key: string]: any;
  }

  export default function merge(...args): MergedObject;
}
