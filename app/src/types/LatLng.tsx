
export default class LatLng implements LatLng {
  public latitude = 0;
  public longitude = 0;

  constructor(lat: number, lng: number) {
    this.latitude = lat;
    this.longitude = lng;
  }
}
