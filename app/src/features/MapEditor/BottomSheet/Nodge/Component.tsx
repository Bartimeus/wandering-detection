import React from 'react';
import { StyleSheet, View } from 'react-native';

const style = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  nodge: {
    position: 'absolute',
    height: 4,
    width: 30,
    backgroundColor: 'rgba(192,192,192,0.8)',
    borderRadius: 2,
    alignItems: 'center',
    top: 10,
  }
});

export default function Nodge() {
  return (
    <View style={style.container}>
      <View style={style.nodge} />
    </View>
  );
}