import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

const styles = StyleSheet.create({
  container: {
    marginLeft: 18,
    marginRight: 18,
  },
});

export default function NoUsers() {
  return (
    <View style={styles.container}>
      <Text>Geen cliënten gevonden.</Text>
    </View>
  );
}
