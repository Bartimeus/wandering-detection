import { gql } from 'apollo-server-express';

export default gql`
  """
  A point in time as described by the [ISO
  8601](https://en.wikipedia.org/wiki/ISO_8601) standard. May or may not include a timezone.
  """
  scalar DateTime

  """
  All geography XY types implement this interface
  """
  interface GeographyGeometry {
    """
    Converts the object to GeoJSON
    """
    geojson: GeoJSON

    """
    Spatial reference identifier (SRID)
    """
    srid: Int!
  }

  """
  All geography types implement this interface
  """
  interface GeographyInterface {
    """
    Converts the object to GeoJSON
    """
    geojson: GeoJSON

    """
    Spatial reference identifier (SRID)
    """
    srid: Int!
  }

  type GeographyLineString implements GeographyInterface & GeographyGeometry {
    geojson: GeoJSON
    srid: Int!
    points: [GeographyPoint]
  }

  type GeographyPoint implements GeographyInterface & GeographyGeometry {
    geojson: GeoJSON
    srid: Int!
    longitude: Float!
    latitude: Float!
  }

  type GeographyPolygon implements GeographyInterface & GeographyGeometry {
    geojson: GeoJSON
    srid: Int!
    exterior: GeographyLineString
    interiors: [GeographyLineString]
  }

  """
  The GeoJSON scalar type represents GeoJSON values as specified by[RFC 7946](https://tools.ietf.org/html/rfc7946).
  """
  scalar GeoJSON
`;
