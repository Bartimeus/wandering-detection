import React, { Component } from 'react';
import { Animated, Dimensions, StatusBar, StyleSheet, View } from 'react-native';
import SlidingUpPanel from 'rn-sliding-up-panel';

export interface IBottomSheetState {
  draggableRange: { top: number, bottom: number };
  allowDragging: boolean;
  initialized: boolean;
}

export interface IBottomSheetProps {
  children?: JSX.Element;
  panelHeight: number;
}

export const bottomSheetStyles = StyleSheet.create({
  container: {
    width: '100%',
  },
  bottomSheetStyling: {
    height: 100,
  },
  content: {
    flex: 1,
    backgroundColor: 'white',
    position: 'relative',
    borderRadius: 10,
    height: 100,
  },
});

export default class BottomSheet<
  P extends IBottomSheetProps,
  S extends IBottomSheetState
  > extends Component<P, S> {
  protected _height: number = Dimensions.get('window').height;
  protected _animatedValue: Animated.Value = new Animated.Value(0);
  protected _slidingUpPanel: SlidingUpPanel | null = null;
  protected _statusBarHidden: boolean = false;
  protected _containerStyles: { width: string, top?: number } = {
    ...bottomSheetStyles.container,
    top: this._height,
  };

  constructor(props: P) {
    super(props);

    // For now we use this tslint to fix the extending component issue.
    /* tslint:disable */
    this.state = {
      allowDragging: true,
      draggableRange: { top: this._height, bottom: 0 },
      initialized: false,
    } as S;
    /* tslint:enable */

    this._animatedValue.addListener(({ value }) => {
      if (value >= this._height - 7) {
        StatusBar.setHidden(true);
        this._statusBarHidden = true;
      } else if (this._statusBarHidden) {
        StatusBar.setHidden(false);
      }
    });
  }

  public async initBottomSheet() {
    if (!this.state.initialized) {
      this.openBottomSheet(true, this.props.panelHeight).then(() => {
        this.setState({
          draggableRange: {
            top: this.state.draggableRange.top,
            bottom: this.props.panelHeight
          },
          initialized: true,
        });
      }).catch(() => {
        // Do nothing.
      });
    }
  }

  public openBottomSheet(open: boolean, height: number): Promise<boolean> {
    return new Promise((resolve, reject) => {
      if (this._slidingUpPanel) {
        if (open) {
          const openListener = ({ value }: { value: number }) => {
            if (value === height) {
              this._animatedValue.removeListener('openListener');
              return resolve(true);
            }
          };
          this._animatedValue.addListener(openListener);
          this._slidingUpPanel.show(height);
        } else {
          this.setState({
            allowDragging: false,
            draggableRange: { top: this.state.draggableRange.top, bottom: 0 }
          }, () => {
            const closeListener = ({ value }: { value: number }) => {
              if (value === 0) {
                this._animatedValue.removeListener('closeListener');
                return resolve(true);
              }
            };
            this._animatedValue.addListener(closeListener);
            this._slidingUpPanel!.hide();
          });
        }
      }
    });

  }

  public render() {
    return (
      <View style={this._containerStyles}>
        <SlidingUpPanel
          showBackdrop={false}
          height={this._height}
          containerStyle={bottomSheetStyles.bottomSheetStyling}
          draggableRange={this.state.draggableRange}
          animatedValue={this._animatedValue}
          allowDragging={true}
          ref={c => this._slidingUpPanel = c}>
          <View style={bottomSheetStyles.content}>
            {this.props.children}
          </View>
        </SlidingUpPanel >
      </View>);
  }
}
