import React, { useEffect } from 'react';
import { StyleSheet, View } from 'react-native';
import SplashScreen from 'react-native-splash-screen';

import MapEditor from './features/MapEditor/Component';

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
  },
  panel: {
    backgroundColor: 'white',
    flex: 1,
    position: 'relative',
  },
});

export default () => {
  useEffect(() => {
    SplashScreen.hide();
  });

  return (
    <View style={styles.container}>
      <MapEditor />
    </View>
  );
};
