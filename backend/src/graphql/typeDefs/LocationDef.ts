import { gql } from 'apollo-server-express';

export default gql`
  type Location {
    location_id: String
    created_on: DateTime
    location: GeographyPoint
    client_id: String
  }
`;
